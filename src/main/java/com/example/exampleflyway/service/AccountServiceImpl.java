package com.example.exampleflyway.service;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import com.example.exampleflyway.api.AccountService;
import com.example.exampleflyway.dto.AccountDTO;
import com.example.exampleflyway.entity.AccountEntity;
import com.example.exampleflyway.repository.AccountRepository;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private DataSource dataSource;

    @PostConstruct
    public void init() {
        System.out.println();
    }

    @Override
    @Transactional
    public AccountDTO save(AccountDTO accountDTO) {

        AccountEntity accountEntity = modelMapper.map(accountDTO, AccountEntity.class);
        //тут какая-то логика
        AccountEntity savedAccount = accountRepository.save(accountEntity);

        //тут еще какая-то логика

        return modelMapper.map(savedAccount, AccountDTO.class);
    }
}
