package com.example.exampleflyway.api;

import com.example.exampleflyway.dto.AccountDTO;

public interface AccountService {
    AccountDTO save(AccountDTO accountDTO);
}
